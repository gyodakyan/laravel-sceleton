<div class="sidebar" data-active-color="blue" data-background-color="black" data-image="/images/img/sidebar-1.jpg">
    <div class="logo">
        <a href="/" class="simple-text">
            InScope LLC
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="/" class="simple-text">
            InS
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{Auth::user()->getAvatar()}}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    {{Auth::user()->fullname()}}
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="/users/profile">My Profile</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav">
            <li {{ (Request::is('') ? 'class=active' : '') }}>
                <a href="/">
                    <i class="material-icons">insert_chart</i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li {{ (Request::is('knowledge') ? 'class=active' : '') }}>
                <a href="/doctors">
                    <i class="material-icons">group</i>
                    <p>Users</p>
                </a>
            </li>

        </ul>
    </div>
</div>
