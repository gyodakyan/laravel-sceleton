<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User;
        $user->email = 'admin@gmail.com';
        $user->password = Hash::make('admin');
        $user->role_id = ADMIN;
        $user->save();
    }
}
