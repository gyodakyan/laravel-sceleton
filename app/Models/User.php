<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\FilterTrait;

class User extends Authenticatable
{
    use Notifiable, FilterTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $role
     * @return bool
     */
    public function roleIs($role)
    {
        return $role === $this->role_id;
    }


    /**
     * @return array
     */
    public static function getRoles()
    {
        return [
            ['title' => 'Admin', 'role_id' => ADMIN]
        ];
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        $img = $this->avatar;

        if (!empty($img)) {
            $url = asset('/avatars/' . $img);
            $path = public_path() . '/avatars/' . $img;

            if (file_exists($path)) {
                return $url;
            }
        }

        return asset('/images/img/default-avatar.png');
    }

    /**
     * @return string
     */
    public function fullName()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * @return string
     */
    public function shortName()
    {
        if (empty($this->last_name)) {
            return ucfirst($this->first_name);
        }

        return ucfirst($this->first_name[0]) . '. ' . ucfirst($this->last_name);
    }

}
