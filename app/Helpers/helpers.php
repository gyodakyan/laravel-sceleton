<?php

/**
 * @param $url
 * @param string $sort_field
 * @return string
 */
function sort_url($url, $sort_field = '')
{
    $params = \Illuminate\Support\Facades\Request::input();

    $dir = array_get($params, 'dir', "ASC");

    if (!$dir) {
        $dir = "ASC";
    } else {
        $dir = ($dir == "ASC") ? "DESC" : "ASC";
    }

    $sort = [
        'sort' => $sort_field,
        'dir' => $dir
    ];

    $query = http_build_query(array_merge($params, $sort));

    return $url . '?' . $query;
}

/**
 * @return array
 */
function get_sort_fields()
{
    $fields = [];
    $params = \Illuminate\Support\Facades\Request::input();

    $sort = array_get($params, 'sort', '');
    $dir = array_get($params, 'dir', '');

    if (!empty($sort)) {
        $fields['sort'] = $sort;
    }

    if (!empty($dir)) {
        $fields['dir'] = $dir;
    }

    return $fields;
}