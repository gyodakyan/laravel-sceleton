<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeRequestCommand extends GeneratorCommand
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Eloquent model class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:requests {name} {path?}';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (parent::fire() === false) {
            return;
        }


    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/requests.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {        
        $path = $this->argument('path');

        $path = empty($path) ? '' : '\\' . $path;
        
        return $rootNamespace.'\Http\Requests' . $path;
    } 

}
