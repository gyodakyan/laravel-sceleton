@extends('layouts.auth')
@section('content')
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="/images/img/login.jpeg">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form method="POST" action="{{ url('/login') }}">
                                {{ csrf_field() }}
                                <div class="card card-login card-hidden">
                                    <div class="card-header text-center" data-background-color="blue">
                                        <h4 class="card-title">Login</h4>
                                    </div>
                                    <div class="card-content">
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                            <div class="label-floating form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="control-label visible-ie8 visible-ie9">Email</label>
                                                <input class="form-control form-control-solid placeholder-no-fix" autofocus type="text" autocomplete="off" name="email" value="{{ old('email') }}" />
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                            <div class="label-floating form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label class="control-label visible-ie8 visible-ie9">Password</label>
                                                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"name="password"/>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember"> Remember me
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-md-12">
                                            <a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Forgot Password?</a>
                                        </div>

                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">Let's go</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
