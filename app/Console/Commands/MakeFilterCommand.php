<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeFilterCommand extends GeneratorCommand
{  

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Filter for model';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:filter {name} {path?}';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';
   
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (parent::fire() === false) {
            return;
        }


    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/filter.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {           
        $path = $this->argument('path');

        $path = empty($path) ? '' : '\\' . $path;

        return $rootNamespace.'\Http\Filters' . $path;
    }
    
}
