<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>InScope LLC</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/material-dashboard.css" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
</head>
<body>

@yield('content')


        <!--   Core JS Files   -->
<script src="/js/plugins/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="/js/plugins/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/plugins/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/plugins/material.min.js" type="text/javascript"></script>
<script src="/js/plugins/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->

<script src="/js/plugins/moment.min.js"></script>
<script src="/js/plugins/chartist.min.js"></script>
<script src="/js/plugins/jquery.bootstrap-wizard.js"></script>
<script src="/js/plugins/bootstrap-notify.js"></script>
<script src="/js/plugins/bootstrap-datetimepicker.js"></script>
<script src="/js/plugins/nouislider.min.js"></script>
<script src="/js/plugins/jquery.select-bootstrap.js"></script>
<script src="/js/plugins/sweetalert2.js"></script>
<script src="/js/plugins/jasny-bootstrap.min.js"></script>
<script src="/js/plugins/jquery.tagsinput.js"></script>
<script src="/js/plugins/material-dashboard.js"></script>

<script>

    $().ready(function() {
        $page = $('.full-page');
        image_src = $page.data('image');

        if(image_src !== undefined){
            image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

</html>