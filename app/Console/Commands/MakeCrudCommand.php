<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeCrudCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:crud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Eloquent model class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $name = Str::studly(class_basename($this->argument('name')));
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));
        $fields = [];

        $this->call('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);

        if ($this->confirm('Please add fields in migration file now, ty?')) {

            $this->info('We trust you but will check anyway...');

            $files = File::allFiles(base_path() . '/database/migrations/');
            $migration = null;
            foreach ($files as $file) {
                if (strpos($file, "create_{$table}_table")) {
                    $migration = $file;
                    break;
                }
            }

            if ($migration) {
                $migration_text = file_get_contents($migration);
                if (substr_count($migration_text, '$table') === 3) {
                    File::delete($migration);
                    exit;
                }



                preg_match_all('/->.+;/', $migration_text, $matches);
                foreach ($matches[0] as $match) {
                    $field_name = substr($match, strpos($match, "('") + 2, strpos($match, ")") - 3 - strpos($match, "('"));
                    $field_type = substr($match, strpos($match, '>') + 1, strpos($match, '(') - 2);
                    if (!in_array($field_type, ['increments', 'timestamps', 'softDeletes'])) {
                        $fields[] = [
                            'name' => $field_name,
                            'type' => $field_type
                        ];
                    }

                }
                $this->info('Ok, seems you have added fields');
            }
        }

        $this->call('make:views', [
            'name' => 'index',
            'folder' => $name,
            'fields' => $fields
        ]);

        $this->call('make:views', [
            'name' => 'create',
            'folder' => $name,
            'fields' => $fields
        ]);

//        $this->call('make:views', [
//            'name' => 'show',
//            'folder' => $name,
//        'fields' => $fields
//        ]);

        $this->call('make:views', [
            'name' => 'edit',
            'folder' => $name,
            'fields' => $fields
        ]);

        $this->call('make:models', [
            'name' => $name
        ]);

        $this->call('make:requests', [
            'name' => "{$name}DeleteRequest",
            'path' => $name
        ]);

        $this->call('make:requests', [
            'name' => "{$name}ReadRequest",
            'path' => $name
        ]);

        $this->call('make:requests', [
            'name' => "{$name}StoreRequest",
            'path' => $name
        ]);

        $this->call('make:requests', [
            'name' => "{$name}UpdateRequest",
            'path' => $name
        ]);

        $this->call('make:filter', [
            'name' => "{$name}Filter",
        ]);

        $this->call('make:controllers', [
            'name' => $name,
            'fields' => $fields
        ]);

        //Append route
        $route_file = $this->laravel['path.base'] . '/routes/web.php';
               $route_txt = "Route::resource('/" . strtolower($name) . "', '" . ucfirst($name) ."Controller');";

        file_put_contents($route_file, $route_txt . PHP_EOL, FILE_APPEND | LOCK_EX);


    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/stubs/readRequest.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }
}
