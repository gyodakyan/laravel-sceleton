<input type="hidden" id="auth-user-id" value="{{ Auth::id()}}">
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">

        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="btn btn-round btn-white btn-fill btn-just-icon dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">content_paste</i>
                        <span id="todos-notifications-count" class="notification">0</span>
                        <p class="hidden-lg hidden-md">
                            TODO`s
                            <b class="caret"></b>
                        </p>
                    </a>
                    <ul id="todos-notifications" class="dropdown-menu  ps-child">
                        <li><a href="#">Nothing here...</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="btn btn-round btn-white btn-fill btn-just-icon dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">notifications</i>
                        <span id="tasks-notifications-count" class="notification">0</span>
                        <p class="hidden-lg hidden-md">
                            TESTS
                            <b class="caret"></b>
                        </p>
                    </a>
                    <ul id="tasks-notifications" class="dropdown-menu ps-child">
                        <li><a href="#">Nothing here...</a></li>
                    </ul>
                </li>
                <li>
                    <a class="btn btn-round btn-white btn-fill btn-just-icon " href="/logout">
                        <i class="material-icons">vpn_key</i>
                        <p class="hidden-lg hidden-md">Logout</p>
                    </a>
                </li>
                <li class="separator hidden-lg hidden-md"></li>
            </ul>
        </div>
    </div>
</nav>

<ul class="hidden hidden-xs" id="notification-template">
    <li class="task-notification-row">
        <a class="task-notification-link" target="_blank">
            <i class="material-icons">content_paste</i>
            <span class="task-notification-title"></span>
            <span class="task-notification-action label label-primary">TODO</span>
        </a>
    </li>
</ul>