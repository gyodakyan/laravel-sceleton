<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>InScope LLC</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="/css/material-dashboard.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Medical') }}</title>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
@include('vendor.sidebar')
<div class="main-panel">
    @include('vendor.navbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
</div>


<script src="//js.pusher.com/3.0/pusher.min.js"></script>

<!--   Core JS Files   -->
<script src="/js/plugins/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="/js/plugins/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/plugins/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/plugins/material.min.js" type="text/javascript"></script>
<script src="/js/plugins/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->

<script src="/js/plugins/moment.min.js"></script>
<script src="/js/plugins/chartist.min.js"></script>
<script src="/js/plugins/jquery.bootstrap-wizard.js"></script>
<script src="/js/plugins/bootstrap-notify.js"></script>
<script src="/js/plugins/bootstrap-datetimepicker.js"></script>
<script src="/js/plugins/nouislider.min.js"></script>
<script src="/js/plugins/jquery.select-bootstrap.js"></script>
<script src="/js/plugins/sweetalert2.js"></script>
<script src="/js/plugins/jasny-bootstrap.min.js"></script>
<script src="/js/plugins/jquery.tagsinput.js"></script>
<script src="/js/plugins/material-dashboard.js"></script>
<script type="text/javascript" src="/js/plugins/summernote.min.js"></script>
<script type="text/javascript" src="/js/plugins/croppie.min.js"></script>

<!-- Custom Scripts -->
<script src="/js/app.js"></script>
<script src="/js/tasks.js"></script>
<script src="/js/board.js"></script>
<script src="/js/finance.js"></script>
<script src="/js/dev.js"></script>
<script src="/js/knowledge.js"></script>

</body>
</html>
