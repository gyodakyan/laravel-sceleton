<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeViewCommand extends GeneratorCommand
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create View';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:views {name} {folder} {fields}';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $name = $this->parseName($this->getNameInput());

        $path = $this->getPath($name);


        if ($this->alreadyExists($this->getNameInput())) {
            $this->error($this->type . ' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildFile($name));

        $this->info($this->type . ' created successfully.');
    }

    /**
     * Build the class with the given name.
     *
     * @param  string $name
     * @return string
     */
    private function buildFile($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceVariables($stub, $name);
    }


    /**
     * Replace the namespace for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     * @return $this
     */
    protected function replaceVariables(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name) . '\\', '', $name);
        $folder = $this->argument('folder');
        $fields = $this->argument('fields');

        if ($class == 'index') {
            $data_headers = '';
            $data_values = '';
            if (is_array($fields) && count($fields)) {
                foreach ($fields as $field) {
                    $data_headers .= '<th><a href="' . '{{sort_url("DummyViewPath", "' . $field['name'] . '")}}">' . ucfirst($field['name']) . "</a></th>" . PHP_EOL . str_repeat("\t", 7);
                    $data_values .= '<td>{{$row->' . $field['name'] . "}}</td>" . PHP_EOL . str_repeat("\t", 6);
                }
            }

            $stub = str_replace('DummyTableHeaders', $data_headers, $stub);
            $stub = str_replace('DummyTableValues', $data_values, $stub);
            $stub = str_replace('DummyTableColumnsCount', count($fields) + 2, $stub);
        }

        if ($class == 'create') {
            $str = '';
            if (is_array($fields) && count($fields)) {
                foreach ($fields as $field) {
                    $str .= str_repeat("\t", 6) . '<!--  ' . $field['name'] . ' -->' . PHP_EOL;
                    $str .= str_repeat("\t", 6) . '<div class="col-md-4 col-sm-6 col-xs-12">' . PHP_EOL;
                    $str .= str_repeat("\t", 7) . '<div class="label-floating form-group{{ $errors->has("'.$field['name'].'") ? \' has-error\' : \'\' }}" >' . PHP_EOL;
                    $str .= str_repeat("\t", 8) . '<label class="control-label" >* ' . ucfirst($field['name']) . ' </label >' . PHP_EOL;
                    $str .= str_repeat("\t", 8) . '<input class="form-control form-control-solid placeholder-no-fix" type = "text" name = "' . $field['name'] . '" value = "{{ old("' . $field['name'] . '") }}" />' . PHP_EOL;
                    $str .= str_repeat("\t", 9) . '@if ($errors->has("' . $field['name'] . '"))' . PHP_EOL;
                    $str .= str_repeat("\t", 10) . '<span class="help-block" >' . PHP_EOL;
                    $str .= str_repeat("\t", 11) . '<strong >{{$errors->first("' . $field['name'] . '") }}</strong >' . PHP_EOL;
                    $str .= str_repeat("\t", 10) . '</span>' . PHP_EOL;
                    $str .= str_repeat("\t", 9) . '@endif' . PHP_EOL;
                    $str .= str_repeat("\t", 7) . '</div>' . PHP_EOL;
                    $str .= str_repeat("\t", 6) . '</div>' . PHP_EOL . PHP_EOL;
                }

                $stub = str_replace('DummyCreateFields', $str, $stub);
            }
        }

        if ($class == 'edit') {
            $str = '';
            if (is_array($fields) && count($fields)) {
                foreach ($fields as $field) {
                    $str .= str_repeat("\t", 6) . '<!--  ' . $field['name'] . ' -->' . PHP_EOL;
                    $str .= str_repeat("\t", 6) . '<div class="col-md-4 col-sm-6 col-xs-12">' . PHP_EOL;
                    $str .= str_repeat("\t", 7) . '<div class="label-floating form-group{{ $errors->has("'.$field['name'].'") ? \' has-error\' : \'\' }}" >' . PHP_EOL;
                    $str .= str_repeat("\t", 8) . '<label class="control-label" >* ' . ucfirst($field['name']) . ' </label >' . PHP_EOL;
                    $str .= str_repeat("\t", 8) . '<input class="form-control form-control-solid placeholder-no-fix" type = "text" name = "' . $field['name'] . '" value = "{{ $DummyViewPath->' . $field['name'] . '}}" />' . PHP_EOL;
                    $str .= str_repeat("\t", 9) . '@if ($errors->has("' . $field['name'] . '"))' . PHP_EOL;
                    $str .= str_repeat("\t", 10) . '<span class="help-block" >' . PHP_EOL;
                    $str .= str_repeat("\t", 11) . '<strong >{{$errors->first("' . $field['name'] . '") }}</strong >' . PHP_EOL;
                    $str .= str_repeat("\t", 10) . '</span>' . PHP_EOL;
                    $str .= str_repeat("\t", 9) . '@endif' . PHP_EOL;
                    $str .= str_repeat("\t", 7) . '</div>' . PHP_EOL;
                    $str .= str_repeat("\t", 6) . '</div>' . PHP_EOL . PHP_EOL;
                }

                $stub = str_replace('DummyEditFields', $str, $stub);
            }
        }


        $stub = str_replace('DummyViewPath', strtolower($folder), $stub);

        return $stub;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $name = strtolower($this->argument('name'));

        return __DIR__ . "/stubs/{$name}.stub";
    }

    /**
     * Get the destination class path.
     *
     * @param  string $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = str_replace($this->laravel->getNamespace(), '', $name);

        $folder = strtolower($this->argument('folder'));

        return $this->laravel['path.base'] . "/resources/views/pages/{$folder}/" . str_replace('\\', '/', $name) . '.blade.php';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    // protected function getDefaultNamespace($rootNamespace)
    // {   
    //    // $path = $this->argument('path');
    //    // $name = $this->argument('name');

    //     //$path = empty($path) ? '' : '\\' . $path;

    //   // return '\resources\views\pages';

    //     //return '\resources\views\pages' .$name;

    // }

}
